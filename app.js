const express = require("express");
const bodyParser = require("body-parser");

const app = express();
const PORT = process.env.PORT || 5000;

app.use(bodyParser.json());

//API routes and logic
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

// POST method route
app.post("/bfhl", (req, res) => {
  try {
    const data = req.body.data;
    // Sample response
    const response = {
      is_success: true,
      user_id: "john_doe_17091999",
      email: "john@xyz.com",
      roll_number: "ABCD123",
      numbers: data.filter((item) => !isNaN(item)),
      alphabets: data.filter(
        (item) =>
          typeof item === "string" &&
          item.length === 1 &&
          /^[A-Za-z]+$/.test(item)
      ),
      highest_alphabet: data
        .filter(
          (item) =>
            typeof item === "string" &&
            item.length === 1 &&
            /^[A-Za-z]+$/.test(item)
        )
        .sort()[0],
    };

    res.status(200).json(response);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// GET method route
app.get("/bfhl", (req, res) => {
  const response = {
    operation_code: 1,
  };
  res.status(200).json(response);
  console.log(response);
});
